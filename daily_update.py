import sys
if len(sys.argv) > 1 and sys.argv[1] == 'env':
    """ 
    Sets the environmental variables when run locally.
    'make_env' package is not distributed.
    """
    import make_env
    make_env.set_env()

from jinja2 import FileSystemLoader, Environment

from src.app import app
from src.models.stocks.stock import Stock
from src.models.users.user import User
from src.common.database import Database

import pytz
import os
import smtplib

from datetime import datetime
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

__author__ = 'scul@gitlab.com'
__copyright__ = "Copyright 2018, scul@gitlab.com"
__license__ = "GPLv3 or later"


def alert(stock_alerts, last_close):
    for user in User.get_all():
        if user.sub:  # only send updates if user is subscribed to email updates.
            send = False
            alerts = []
            alerts.append(f'  Symbol  | Last Close | 200 day avg')
            for stock in stock_alerts:
                if stock.symbol in user.stocks:
                    send = True
                    symbol = f'{stock.symbol}'.center(10)
                    last = f'${stock.price_history[last_close]:.3f}'.center(12)
                    avg = f'${stock.moving_avg:.3f}'.center(12)
                    alerts.append(f'{symbol}|{last}|{avg}')

            if send:
                alert_text = "\n".join(alerts)

                alert_text = f'{user.name},\n\nALERT from arf-market-watch!\n\n{alert_text}\n\n' \
                             f'Unsubscribe here https://arf-market-watch.herokuapp.com/users/profile'

                alerts = [alert.split('|') for alert in alerts]

                file_loader = FileSystemLoader('src/templates/emails/')
                env = Environment(loader=file_loader)
                template = env.get_template('email.jinja2')
                alert_html = template.render(user=user.name, alerts=alerts)

                sender = 'arf.market.watch@gmail.com'
                recipient = user.email

                msg = MIMEMultipart('alternative')
                msg['Subject'] = 'Stock Alert'
                msg['From'] = sender
                msg['Bcc'] = recipient

                text = MIMEText(alert_text, 'plain')
                html = MIMEText(alert_html, 'html')

                msg.attach(text)
                msg.attach(html)

                s = smtplib.SMTP_SSL('smtp.gmail.com', 465)
                password = os.environ['marketWatchPassword']
                s.login("arf.market.watch", password)
                s.sendmail(sender, recipient, msg.as_string())
                s.quit()
    

def main():
    Database.initialize()

    stock_alerts = []
    stocks = Stock.get_all()
    today = datetime.now(tz=pytz.timezone('America/New_York')).strftime('%Y-%m-%d')
    for stock in stocks:
        stock.add_date_price()
        stock.update_average()
        stock.trim()

        last_close = list(stock.price_history.keys())[0]
        if last_close == today and stock.price_history[last_close] < stock.moving_avg:
            stock_alerts.append(stock)

    if len(stock_alerts) > 0:
        alert(stock_alerts, last_close)


if __name__ == '__main__':
    main()
    print('Daily update complete.')
