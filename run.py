from src.app import app

__author__ = 'scul@gitlab.com'
__copyright__ = "Copyright 2018, scul@gitlab.com"
__license__ = "GPLv3 or later"

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)