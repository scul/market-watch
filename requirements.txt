bcrypt==3.1.4
certifi==2018.8.24
cffi==1.11.5
chardet==3.0.4
click==6.7
Flask==1.0.2
flask-paginate==0.5.1
gunicorn==19.9.0
idna==2.7
itsdangerous==0.24
Jinja2==2.10
MarkupSafe==1.0
passlib==1.7.1
pycparser==2.18
pymongo==3.7.1
pytz==2018.5
requests==2.19.1
six==1.11.0
urllib3==1.23
Werkzeug==0.14.1
