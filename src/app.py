from flask import Flask, render_template

import os
import sys

if len(sys.argv) > 1 and sys.argv[1] == 'env':
    """ 
    Sets the environmental variables when run locally.
    'make_env' package is not distributed.
    """
    import make_env
    make_env.set_env()


__author__ = 'scul@gitlab.com'


app = Flask(__name__)
app.config.from_object('src.config')

app.secret_key = os.environ['SECRET_KEY']

from src.common.database import Database


@app.before_first_request
def init_db():
    Database.initialize()


@app.route('/')
def index():
    return render_template('home.jinja2')


from src.models.users.views import user_blueprint
from src.models.stocks.views import stock_blueprint

app.register_blueprint(user_blueprint, url_prefix='/users')
app.register_blueprint(stock_blueprint, url_prefix='/stocks')
