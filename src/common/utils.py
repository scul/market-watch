import re
from passlib.hash import bcrypt_sha256

__author__ = 'scul@gitlab.com'
__copyright__ = "Copyright 2018, scul@gitlab.com"
__license__ = "GPLv3 or later"


class Utils:
    @staticmethod
    def verify_password(password, hashed_password):
        return bcrypt_sha256.verify(password, hashed_password)

    @staticmethod
    def hash(data):
        return bcrypt_sha256.hash(data)

    @staticmethod
    def email_is_valid(email):
        email_matcher = re.compile('^[\w\-+.]+@([\w\-]+\.)+[\w]+$')
        return True if email_matcher.match(email) else False
