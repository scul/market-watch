from functools import wraps

from flask import session, flash, redirect, url_for, request

__author__ = 'scul@gitlab.com'
__copyright__ = "Copyright 2018, scul@gitlab.com"
__license__ = "GPLv3 or later"


def requires_login(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if 'name' not in session.keys() or session['name'] is None:
            flash(u'You need to be signed in for this page.', 'info')
            return redirect(url_for('users.login', next=request.path))
        return func(*args, **kwargs)
    return decorated_function


def requires_logout(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if 'name' in session.keys() and session['name'] is not None:
            return redirect(url_for('users.stocks'))
        return func(*args, **kwargs)
    return decorated_function


def disabled(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        flash(f'{request.path} is disabled.', 'info')
        return redirect(url_for('index'))
    return decorated_function
