import uuid
import re

import src.models.users.errors as UserErrors
import src.models.users.constants as UserConstants
from src.common.database import Database
from src.common.utils import Utils
from src.models.stocks.stock import Stock

__author__ = 'scul@gitlab.com'
__copyright__ = "Copyright 2018, scul@gitlab.com"
__license__ = "GPLv3 or later"


class User:
    def __init__(self, name, email, password_hash, sub=None, stocks=None, timezone=None, _id=None):
        self.name_ = name
        self.email = email
        self.password_hash = password_hash
        self.sub = sub if sub is not None else True
        self.stocks = set(stock for stock in stocks.split(',')) if stocks else set()
        self.timezone = timezone if timezone else 'UTC'
        self._id = _id if _id else uuid.uuid4().hex

    def __repr__(self):
        return '<User {}>'.format(self.name)

    @property
    def name(self):
        return self.name_

    @name.setter
    def name(self, name):
        if self.is_name_used(name):
            raise UserErrors.UserNameUsedError(f'User name {name} has already been used')
        match = re.search(r'^[A-Za-z0-9\s]+$', name)
        if match:
            self.name_ = name
        else:
            raise AttributeError('Invalid characters')
        self.save_to_mongo()

    @property
    def password(self):
        raise AttributeError('Password is not accessible')

    @password.setter
    def password(self, passwd):
        self.password_hash = Utils.hash(passwd)
        self.save_to_mongo()

    @staticmethod
    def is_name_used(name):
        user_data = Database.find_one(UserConstants.COLLECTION, {'name': name})
        return user_data is not None  # return true if name is used, false if not.

    @staticmethod
    def is_login_valid(name, password):
        user_data = Database.find_one(UserConstants.COLLECTION, {'name': name})
        if user_data is None:
            raise UserErrors.InvalidUserError('Invalid user')
        if not Utils.verify_password(password, user_data['password_hash']):
            raise UserErrors.InvalidPasswordError('Invalid Password')
        return True

    @staticmethod
    def register(name, email, password, password2, sub, timezone):
        user_data = Database.find_one(UserConstants.COLLECTION, {'name': name})

        if user_data is not None:
            raise UserErrors.UserNameUsedError('Username is already used')
        if not Utils.email_is_valid(email):
            raise UserErrors.EmailInvalidError('Malformed e-mail')
        if not password == password2:
            raise UserErrors.PasswordMatchError('Passwords do not match')
        if len(password) < UserConstants.MINPASSWORDLEN:
            raise UserErrors.PasswordLengthError('Password is too short')

        User(name, email, Utils.hash(password), sub=sub, timezone=timezone).save_to_mongo()

        return True

    def save_to_mongo(self):
        Database.update(UserConstants.COLLECTION, {'_id': self._id}, self.json())

    def json(self):
        return {
            '_id': self._id,
            'name': self.name,
            'password_hash': self.password_hash,
            'sub': self.sub,
            'email': self.email,
            'stocks': self.string_stocks(),
            'timezone': self.timezone
        }

    def delete_account(self):
        Database.remove(UserConstants.COLLECTION, {'_id': self._id})

    @classmethod
    def get_by_name(cls, name):
        return cls(**Database.find_one(UserConstants.COLLECTION, {"name": name}))

    @classmethod
    def get_by_id(cls, _id):
        return cls(**Database.find_one(UserConstants.COLLECTION, {"_id": _id}))

    @classmethod
    def get_all(cls):
        return [cls(**elem) for elem in Database.find(UserConstants.COLLECTION, {})]

    def get_stocks(self):
        return set(Stock.get_by_symbol(symbol) for symbol in self.stocks)

    def add_stock(self, symbol):
        self.stocks.add(symbol)
        self.save_to_mongo()

    def remove_stock(self, symbol):
        self.stocks.remove(symbol)
        self.save_to_mongo()

    def toggle_subscription(self):
        self.sub = False if self.sub==True else True
        self.save_to_mongo()

    def update_email(self, email):
        if Utils.email_is_valid(email):
            self.email = email
            self.save_to_mongo()
        else:
            raise UserErrors.EmailInvalidError('Malformed e-mail')

    def string_stocks(self):
        return ','.join(stock for stock in self.stocks)

    def get_id(self):
        return self._id
