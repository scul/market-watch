__author__ = 'scul@gitlab.com'
__copyright__ = "Copyright 2018, scul@gitlab.com"
__license__ = "GPLv3 or later"


class UserError(Exception):
    def __init__(self, message):
        self.message = message


class InvalidPasswordError(UserError):
    pass


class InvalidUserError(UserError):
    pass


class PasswordMatchError(UserError):
    pass


class PasswordLengthError(UserError):
    pass


class UserNameUsedError(UserError):
    pass


class EmailInvalidError(UserError):
    pass
