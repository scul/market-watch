from flask import Blueprint, request, url_for, session, render_template, flash
from werkzeug.utils import redirect
from src.models.users.user import User
from src.common.utils import Utils
import src.common.decorators as common_decorators
import src.models.users.errors as UserErrors

__author__ = 'scul@gitlab.com'
__copyright__ = "Copyright 2018, scul@gitlab.com"
__license__ = "GPLv3 or later"


user_blueprint = Blueprint('users', __name__)


@user_blueprint.route('/login', methods=['GET', 'POST'])
@common_decorators.requires_logout
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        try:
            if User.is_login_valid(username, password):
                session['name'] = username
                return redirect(url_for('users.stocks'))
        except UserErrors.UserError:
            flash('Username or password incorrect!', 'danger')

    return render_template('users/login.jinja2')


@user_blueprint.route('/logout')
@common_decorators.requires_login
def logout():
    session['name'] = None
    return redirect(url_for('index'))


@user_blueprint.route('/register', methods=['GET', 'POST'])
@common_decorators.requires_logout
def register():
    if request.method == 'POST':
        username = request.form['username']
        email = request.form['email']
        if request.form.getlist('sub'):
            sub = True
        else:
            sub = False
        password = request.form['password']
        password2 = request.form['password2']
        timezone = None

        try:
            if User.register(username, email, password, password2, sub, timezone):
                session['name'] = username
                return redirect(url_for('users.stocks'))
        except UserErrors.UserError as e:
            flash(e.message, 'danger')
    return render_template('users/register.jinja2')


@user_blueprint.route('/profile', methods=['GET', 'POST'])
@common_decorators.requires_login
def profile():
    user = User.get_by_name(session['name'])
    if request.method == 'POST':
        action = request.form['action']
        if action == 'name':
            try:
                user.name = request.form['name']
                session['name'] = user.name
            except (AttributeError, UserErrors.UserNameUsedError) as e:
                flash(e.message, 'danger')

        elif action == 'email':
            try:
                user.update_email(request.form['email'])
            except UserErrors.EmailInvalidError as e:
                flash(e.message, 'danger')

        elif action == 'sub':
            user.toggle_subscription()

        elif action == 'password':
            curr_password = request.form['curr_password']
            passwd1 = request.form['new_password1']
            passwd2 = request.form['new_password2']
            if Utils.verify_password(curr_password, user.password_hash) and passwd1 == passwd2:
                user.password = passwd1
                flash('Password changed', 'success')
            else:
                flash('Invalid password or New passwords do not match', 'danger')

        elif action == 'delete':
            if user.name == request.form['name']:
                user.delete_account()
                flash('Account Deleted', 'success')
                return redirect(url_for('users.logout'))
            else:
                flash('Account name mis-match', 'info')

        return redirect(url_for('users.profile'))
    return render_template('users/profile.jinja2', user=user)


@user_blueprint.route('/stocks')
@common_decorators.requires_login
def stocks():
    user = User.get_by_name(session['name'])
    stocks = sorted(user.get_stocks(), key=lambda stock:stock.symbol)
    return render_template('users/stocks.jinja2', user=user.name, stocks=stocks)
