import uuid
import requests
import re
import json
import datetime

from collections import OrderedDict

import src.models.stocks.constants as StockConstants
import src.models.stocks.errors as StockErrors
from src.common.database import Database

__author__ = 'scul@gitlab.com'
__copyright__ = "Copyright 2018, scul@gitlab.com"
__license__ = "GPLv3 or later"


class Stock:
    def __init__(self, symbol, price_history, dividends, moving_avg=None, _id=None):
        self.symbol = symbol
        self.price_history = OrderedDict(sorted(price_history.items(), key=lambda t: t[0], reverse=True))
        self.dividends = OrderedDict(sorted(dividends.items(), key=lambda t: t[0], reverse=True))
        self.moving_avg = moving_avg if moving_avg else 0
        self._id = _id if _id else uuid.uuid4().hex

    def __repr__(self):
        return f'<Stock {self.symbol}>'

    def __str__(self):
        return self.__repr__()

    @staticmethod
    def new_stock(symbol):
        stock_data = Database.find_one(StockConstants.COLLECTION, {'symbol': symbol})

        if stock_data is not None or symbol == '':
            return

        price_history, dividends = Stock.get_history(symbol)
        tot = sum(v for k,v in list(price_history.items())[:200])
        _avg = tot / 200
        Stock(symbol, price_history, dividends, _avg).save_to_mongo()

    @staticmethod
    def get_history(symbol):
        url = f'https://finance.yahoo.com/quote/{symbol}/history'

        r = requests.get(url)

        re_data = re.search(r'HistoricalPriceStore":(.*?)\],\"', r.text)
        try:
            re_data_clean = f'{re_data.group(1)}]}}'
            # print(re_data_clean)
            json_data = json.loads(re_data_clean)
        except AttributeError:
            raise StockErrors.SymbolError('Cannot find that stock symbol')

        price = OrderedDict()
        div = OrderedDict()
        i = 0
        for data in json_data['prices']:
            if 'close' in data:
                i += 1
                price[datetime.datetime.fromtimestamp(int(data['date'])).strftime('%Y-%m-%d')] = data['close']
            elif 'type' in data and data['type'] == 'DIVIDEND':
                div[datetime.datetime.fromtimestamp(int(data['date'])).strftime('%Y-%m-%d')] = data['amount']
            if i > 200:
                break

        return price, div

    def add_date_price(self):
        price_list, div = self.get_history(self.symbol)
        last_close = list(price_list.keys())[0]
        self.price_history[last_close] = price_list[last_close]
        self.price_history.move_to_end(last_close, last=False)
        self.save_to_mongo()
        # print(self.price_history)
        return last_close

    def update_average(self):
        tot = sum(self.price_history[k] for k in list(self.price_history.keys())[:200])
        self.moving_avg = tot / 200

    def trim(self):
        i = 0
        temp_dict = OrderedDict()
        for k, v in self.price_history.items():
            temp_dict[k] = v
            i += 1
            if i > 200:
                break

    def save_to_mongo(self):
        Database.update(StockConstants.COLLECTION, {'_id': self._id}, self.json())

    def json(self):
        return {
            '_id': self._id,
            'symbol': self.symbol,
            'price_history': self.price_history,
            'dividends': self.dividends,
            'moving_avg': self.moving_avg
        }

    @classmethod
    def get_by_symbol(cls, symbol):
        return cls(**Database.find_one(StockConstants.COLLECTION, {'symbol': symbol}))

    @classmethod
    def get_by_id(cls, _id):
        return cls(**Database.find_one(StockConstants.COLLECTION, {'_id': _id}))

    @classmethod
    def get_all(cls):
        return [cls(**elem) for elem in Database.find(StockConstants.COLLECTION, {})]
