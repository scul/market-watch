__author__ = 'scul@gitlab.com'
__copyright__ = "Copyright 2018, scul@gitlab.com"
__license__ = "GPLv3 or later"


class StockError(Exception):
    def __init__(self, message):
        self.message = message


class SymbolError(StockError):
    pass


class StockPriceError(StockError):
    pass
