from flask import Blueprint, request, session, redirect, url_for, flash
from src.models.users.user import User
from src.models.stocks.stock import Stock
import src.common.decorators as user_decorators
import src.models.stocks.errors as StockErrors


__author__ = 'scul@gitlab.com'
__copyright__ = "Copyright 2018, scul@gitlab.com"
__license__ = "GPLv3 or later"

stock_blueprint = Blueprint('stocks', __name__)


@stock_blueprint.route('/new', methods=['POST'])
@user_decorators.requires_login
def new_stock():
    if request.method == 'POST':
        symbols = [symbol.strip() for symbol in request.form['symbol'].upper().split(',')]
        print(symbols)
        user = User.get_by_name(session['name'])
        for symbol in symbols:
            try:
                Stock.new_stock(symbol)
                if len(symbol.strip()) >= 1:
                    user.add_stock(symbol)
            except (StockErrors.StockError, StockErrors.SymbolError) as e:
                flash(e.message, 'danger')
        return redirect(url_for('users.stocks'))


@stock_blueprint.route('/delete', methods=['POST'])
@user_decorators.requires_login
def remove_stock():
    stock_symbol = request.form['stock_symbol']
    User.get_by_name(session['name']).remove_stock(stock_symbol)
    flash(f'Removed {stock_symbol} from your tracking.', 'info')
    return redirect(url_for('users.stocks'))
